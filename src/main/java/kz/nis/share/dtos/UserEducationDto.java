package kz.nis.share.dtos;


import kz.nis.share.entities.EDegree;

public class UserEducationDto {
    private String universityName;

    private String major;

    private EDegree degree;
}
